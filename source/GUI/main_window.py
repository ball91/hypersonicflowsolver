from appJar import gui
from source.structs.launch_settings import LaunchSettings
from source.BaseManager.launch_settings_storage import StorageForLaunchSettings
from source.utils.observer import Subscriber, Keys
from source.solver.main_solver import MainSolver
from source.BaseManager.result_storage import ResultStorage
from source.plots.main_plot_creator import MainPlotCreator


class MainWindow(Subscriber):
    def __init__(self):
        super().subscribe(Keys().console_out)

    def _browse_button_press(self):
        self.app.setEntry("Working directory",
                          text=self.app.directoryBox(title="Select working folder",
                                                     dirName=LaunchSettings().working_dir),
                          callFunction=False)

    def _print_to_console(self, str):
        self.app.setTextArea("Output", str + "\n")

    def _calc(self):
        """Calculate the problem"""
        self._set_launch_settings_from_window()
        solver = MainSolver()
        solver.start()

    def _set_launch_settings_from_window(self):
        LaunchSettings().working_dir = str(self.app.entry("Working directory"))
        LaunchSettings().mach_inf = float(self.app.entry("Mach number M ="))
        LaunchSettings().gamma = float(self.app.entry("Adiabatic index γ ="))
        LaunchSettings().Sw = float(self.app.entry("Enthalpy on the plate Sw ="))
        LaunchSettings().alpha_n = float(self.app.entry("Influence of not self-similar solution αn = "))
        LaunchSettings().K = float(self.app.entry("Injected speed coefficient K ="))
        LaunchSettings().N = int(self.app.entry("Number of nodes in grid N ="))
        LaunchSettings().etta_max_2 = float(self.app.entry("Maximum coordinate in the second zone η2 ="))
        LaunchSettings().etta_min_2 = float(self.app.entry("Minimum coordinate in the second zone η2 ="))
        LaunchSettings().etta_max_3 = float(self.app.entry("Maximum coordinate in the third zone η3 ="))
        StorageForLaunchSettings().save()

    def _set_settings_in_window(self):
        StorageForLaunchSettings().load()
        ResultStorage().set_working_dir(LaunchSettings().working_dir)
        self.app.setEntry("Working directory", LaunchSettings().working_dir)
        self.app.setEntry("Mach number M =", LaunchSettings().mach_inf)
        self.app.setEntry("Adiabatic index γ =", LaunchSettings().gamma)
        self.app.setEntry("Enthalpy on the plate Sw =", LaunchSettings().Sw)
        self.app.setEntry("Influence of not self-similar solution αn = ", LaunchSettings().alpha_n)
        self.app.setEntry("Injected speed coefficient K =", LaunchSettings().K)
        self.app.setEntry("Number of nodes in grid N =", LaunchSettings().N)
        self.app.setEntry("Maximum coordinate in the second zone η2 =", LaunchSettings().etta_max_2)
        self.app.setEntry("Minimum coordinate in the second zone η2 =", LaunchSettings().etta_min_2)
        self.app.setEntry("Maximum coordinate in the third zone η3 =", LaunchSettings().etta_max_3)

    def _plots(self):
        MainPlotCreator().create()

    def update(self, key, message):
        if key is Keys().console_out:
            self._print_to_console(message)

    def start(self):
        """Start main window render"""
        with gui("HypersonicFlowSolver", "800x700", bg='grey', font={'size': 20}) as self.app:
            self.app.setFont(size=16, family="Times")
            self.app.setButtonFont(size=14, family="Verdana")

            self.app.label("Select working directory", bg='blue', fg='orange')
            self.app.entry("Working directory", label=True, focus=True)
            self.app.button("Browse", self._browse_button_press)

            self.app.label("Known parameters of the task", bg='blue', fg='orange')
            self.app.entry("Mach number M =", label=True, focus=True)
            self.app.entry("Adiabatic index γ =", label=True, focus=True)
            self.app.entry("Enthalpy on the plate Sw =", label=True, focus=True)
            self.app.entry("Influence of not self-similar solution αn = ", label=True, focus=True)
            self.app.entry("Injected speed coefficient K =", label=True, focus=True)

            self.app.label("Requirements for numeric solution", bg='blue', fg='orange')
            self.app.entry("Number of nodes in grid N =", label=True, focus=True)
            self.app.entry("Maximum coordinate in the second zone η2 =", label=True, focus=True)
            self.app.entry("Minimum coordinate in the second zone η2 =", label=True, focus=True)
            self.app.entry("Maximum coordinate in the third zone η3 =", label=True, focus=True)

            self.app.label("Activity", bg='blue', fg='orange')
            self.app.buttons(["Calculate", "Plots", "Exit"], [self._calc, self._plots, self.app.stop])

            self._set_settings_in_window()

            self.app.label("Output", bg='blue', fg='orange')
            self.app.addScrolledTextArea("Output", text="Greeting from HypersonicFlowSolver!\n")
