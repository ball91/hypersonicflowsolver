import os
from source.structs.launch_settings import LaunchSettings
from source.get_launch_dir import get_launch_dir

import pandas as pd
import numpy as np


class StorageForLaunchSettings:
    """Хранение настроек программы"""
    def __init__(self):
        self.path = os.path.join(get_launch_dir(), ".launch_settings.csv")
        self.columns = ["working_dir", "mach_inf", "gamma", "Sw", "alpha_n", "K", "N",
                        "etta_max_2", "etta_min_2", "etta_max_3", "accuracy"]

    def save(self):
        data = np.array([x for x in LaunchSettings().pack_to_tuple()])
        df = pd.DataFrame(columns=self.columns)
        df.loc[0] = data
        df.to_csv(self.path, index=False)

    def load(self):
        if not os.path.exists(self.path):
            return

        df = pd.read_csv(self.path)
        row = df.iloc[0]
        LaunchSettings().unpack_from_tuple(tuple(row.values))
