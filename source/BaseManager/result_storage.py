import os
from source.structs.launch_settings import LaunchSettings
from source.utils.singleton import singleton
from source.get_launch_dir import get_launch_dir

import pandas as pd


@singleton
class ResultStorage:
    def __init__(self):
        self.working_dir = get_launch_dir()  # directory for store data

    def set_working_dir(self, dir):
        """Задать корневую директорию, в которой будут храниться результаты."""
        self.working_dir = dir

    def save_data(self, data_zip, file_name, x=None):
        """
        Сохранение данных в файл формата .csv (старый файл будет перезаписан)
        :param data_zip: данные в виде zip([названия колонок], [списки с данными])
        (все списки в словаре должны быть одной длины)
        :param file_name: имя файла для сохранения (не полный путь, разрешение файла указывать не надо)
        :param x: текущая координата по x (если требуется)
        :return: None
        """
        gamma = LaunchSettings().gamma
        K = LaunchSettings().K
        Sw = LaunchSettings().Sw
        file_name += ".csv"

        path_1 = os.path.join(self.working_dir, "Sw_{}".format(Sw), "gamma_{}".format(gamma),
                              "K_{}".format(K))
        path_2 = os.path.join(self.working_dir, "gamma_{}".format(gamma), "Sw_{}".format(Sw),
                              "K_{}".format(K))

        if x != None:
            path_1 = os.path.join(path_1, "x_{}".format(x))
            path_2 = os.path.join(path_2, "x_{}".format(x))
        if not os.path.exists(path_1):
            os.makedirs(path_1)
        if not os.path.exists(path_2):
            os.makedirs(path_2)

        path_1 = os.path.join(path_1, file_name)
        path_2 = os.path.join(path_2, file_name)

        df = pd.DataFrame()
        for column, data in data_zip:
            df[column] = pd.Series(data)

        df.to_csv(path_1, index=False)
        df.to_csv(path_2, index=False)

    def save_eigenvalue(self, n):
        """
        Сохранение собственного числа (добавление к уже существующему файлу, если он есть.)
        Если заданные значения "Sw", "gamma" и "K" уже имеются, то перезаписываем значение "n"
        """
        gamma = LaunchSettings().gamma
        K = LaunchSettings().K
        Sw = LaunchSettings().Sw
        file_name = os.path.join(self.working_dir, "eigenvalue.csv")
        columns = ["Sw", "gamma", "K", "n"]

        # Создаем файл, если он не существует
        if not os.path.exists(file_name):
            df = pd.DataFrame([[Sw, gamma, K, n]], columns=columns)
        else:
            df = pd.read_csv(file_name)
            # Проверяем есть ли уже запись с такими вводными в базе
            match_indexes = df[(df['Sw'] == Sw) & (df['gamma'] == gamma) & (df['K'] == K)].index.values
            for index in match_indexes:
                df.loc[index, 'n'] = n
            if len(match_indexes) == 0:
                df = df.append(pd.DataFrame([[Sw, gamma, K, n]], columns=columns), ignore_index=True)
        df.to_csv(file_name, index=False)
