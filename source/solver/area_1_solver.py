from source.structs.launch_settings import LaunchSettings
from source.utils.runge_kutta import RungeKutta
from source.utils.func_approximation import func_approximation


class Area1Solver:
    def __init__(self, n):
        self.n = n
        self.N = LaunchSettings().N
        self.h = 1 / self.N
        self.ksi_min = 0
        self.ksi_max = 1

        self.P10 = [0.0] * (self.N + 1)  # Pressure in area 1 in 0 approximation
        self.P1n = [0.0] * (self.N + 1)  # Pressure in area 1 in n approximation
        self.V10 = [0.0] * (self.N + 1)  # Vertical speed in area 1 in 0 approximation
        self.V1n = [0.0] * (self.N + 1)  # Vertical speed in area 1 in n approximation

    def start(self):
        self._set_targeting_values()
        self._calc_0_values()
        self._calc_n_value()

    def _set_targeting_values(self):
        self.P10[self.N] = 1
        self.P1n[self.N] = 0
        self.V10[self.N] = 1
        self.V1n[self.N] = 0
        for n in range(0, self.N):
            self.P10[n] = n * self.h
            self.V10[n] = n * self.h
            self.P1n[n] = 1 - n * self.h
            self.V1n[n] = 1 - n * self.h

    def _calc_0_values(self):
        V10_prev = [0] * (self.N + 1)
        P10_prev = [0] * (self.N + 1)
        while not self._enough_accuracy(self.V10, V10_prev) or not self._enough_accuracy(self.P10, P10_prev):
            V10_prev = self.V10.copy()
            P10_prev = self.P10.copy()
            self._calc_P0()
            self._calc_V0()

    def _calc_P0(self):
        dV10 = [0] * (self.N + 1)
        for n in range(0, self.N):
            dV10[n] = (self.V10[n + 1] - self.V10[n]) / self.h
        dV10[self.N] = dV10[self.N - 1]

        def f(ksi, P0=None):
            return ksi * self._f_simul(ksi, dV10) + self._f_simul(ksi, self.V10) / 3

        for n in range(self.N, 0, -1):
            self.P10[n - 1] = self.P10[n] - self.h * f(n * self.h)

    def _calc_V0(self):
        dP10 = [0] * (self.N + 1)
        for n in range(0, self.N):
            dP10[n] = (self.P10[n + 1] - self.P10[n]) / self.h
        dP10[self.N] = dP10[self.N - 1]

        def f(ksi, V0=None):
            gamma = LaunchSettings().gamma
            return (gamma - 1) / (2 * gamma) * (ksi ** (- 2 / (3 * gamma))) / \
                   (self._f_simul(ksi, self.P10) ** (1 + 1 / gamma)) * \
                   (2 / 3 * self._f_simul(ksi, self.P10) + ksi * self._f_simul(ksi, dP10))

        for n in range(self.N, 0, -1):
            self.V10[n - 1] = self.V10[n] - self.h * f(n * self.h)

    def _calc_n_value(self):
        V1n_prev = [0] * (self.N + 1)
        P1n_prev = [0] * (self.N + 1)
        while not self._enough_accuracy(self.V1n, V1n_prev) or not self._enough_accuracy(self.P1n, P1n_prev):
            V1n_prev = self.V1n.copy()
            P1n_prev = self.P1n.copy()
            self._calc_Pn_and_Vn()

    def _calc_Pn_and_Vn(self):
        for i in range(self.N, 0, -1):
            self.P1n[i - 1] = self.P1n[i] - i * self.h * (self.V1n[i] - self.V1n[i - 1]) + \
                              4 / 3 * self.h * (self.n - 0.25) * self.V1n[i] + \
                              16 / 9 * self.h * self.n * (self.n + 1) * self.V10[i]

            gamma = LaunchSettings().gamma
            C1 = - (gamma - 1) / 2 / gamma * ((self.h * i) ** (1 - 2 / 3 / gamma)) / (self.P10[i] ** (1 + 1 / gamma))
            C2 = (gamma + 1) / gamma * (self.V10[i] - self.V10[i - 1]) / self.h / self.P10[i] + \
                 2 / 3 * (gamma - 1) / gamma * ((self.h * i) ** (-2 / 3 / gamma)) * (self.n - 0.5) / \
                 (self.P10[i] ** (1 + 1 / gamma))
            C3 = 8 / 3 / gamma * (self.n + 1) * ((self.h * i) ** (4 * self.n / 3) - 1) * \
                 (self.V10[i] - self.V10[i - 1]) - \
                 16 / 9 * self.n * (self.n + 1) * (gamma - 1) / gamma * (self.h * i) ** (-2 / 3 / gamma) / \
                 (self.P10[i] ** (1 / gamma))

            self.V1n[i - 1] = self.V1n[i] + C1 * (self.P1n[i] - self.P1n[i - 1]) + C2 * self.h * self.P1n[i] - \
                              C3 * self.h

    @staticmethod
    def _enough_accuracy(list1, list2):
        for v1, v2 in zip(list1, list2):
            if abs(v1 - v2) > LaunchSettings().accuracy:
                return False

        return True

    def _f_simul(self, ksi, values):
        """Simulate function"""
        return func_approximation(ksi, values, self.ksi_min, self.ksi_max)
