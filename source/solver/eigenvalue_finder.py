from source.utils.simpson import Simpson
from source.structs.launch_settings import LaunchSettings
from source.utils.func_approximation import func_approximation


class EigenValueFinder:
    def __init__(self, n, solver1, solver2, solver3):
        self.n = n
        self.P10 = solver1.P10
        self.P1n = solver1.P1n
        self.V10 = solver1.V10
        self.V1n = solver1.V1n
        self.f20 = solver2.f20
        self.f2n = solver2.f2n
        self.S20 = solver2.S20
        self.S2n = solver2.S2n
        self.f30 = solver3.f30
        self.f3n = solver3.f3n

        self.df20 = solver2.u0
        self.df2n = solver2.un
        self.df30 = solver3.u30
        self.df3n = solver3.u3n
        self.special_params = 0
        self.etta3_max = solver3.delta_etta_3
        self.i_max_3 = solver3.i_max

    def find(self, special_params):
        self.special_params = special_params
        a = self._first_integral()
        b = self._second_integral()
        self._recalc_special_params(special_params, a)

        result = self.P10[0] + self.V10[0] / 3 * (4 * self.n + 3) * (1 + b / a) * (self.P1n[0] / self.V1n[0])
        return result

    def _first_integral(self):
        """Calculate first integral"""
        a2 = Simpson(self._find_a2_integrand, LaunchSettings().etta_min_2, LaunchSettings().etta_max_2,
                     LaunchSettings().N).calc()
        a3 = Simpson(self._find_a3_integrand, 0, self.etta3_max, self.i_max_3).calc()
        return a2 + a3

    def _second_integral(self):
        """Calculate second integral"""
        b2 = Simpson(self._find_b2_integrand, LaunchSettings().etta_min_2, LaunchSettings().etta_max_2,
                     LaunchSettings().N).calc()
        b3 = Simpson(self._find_b3_integrand, 0, self.etta3_max, self.i_max_3).calc()
        return b2 + b3

    def _find_a2_integrand(self, etta):
        """Integrand function in first integral for 2 area"""
        etta_min = LaunchSettings().etta_min_2
        etta_max = LaunchSettings().etta_max_2
        return func_approximation(etta, self.S20, etta_min, etta_max) - \
               func_approximation(etta, self.df20, etta_min, etta_max) ** 2

    def _find_a3_integrand(self, etta):
        """Integrand function in first integral for 3 area"""
        etta_min = 0
        etta_max = LaunchSettings().etta_max_3 * LaunchSettings().Sw ** 0.5 * (- self.special_params["Ψw"])
        return LaunchSettings().Sw - \
               func_approximation(etta, self.df30, etta_min, etta_max) ** 2 * LaunchSettings().Sw

    def _find_b2_integrand(self, etta):
        """Integrand function in second integral for 2 area"""
        etta_min = LaunchSettings().etta_min_2
        etta_max = LaunchSettings().etta_max_2
        return 2 * func_approximation(etta, self.df20, etta_min, etta_max) * \
               func_approximation(etta, self.df2n, etta_min, etta_max) - \
               func_approximation(etta, self.S2n, etta_min, etta_max)

    def _find_b3_integrand(self, etta):
        """Integrand function in second integral for 3 area"""
        etta_min = 0
        etta_max = LaunchSettings().etta_max_3 * LaunchSettings().Sw ** 0.5 * (- self.special_params["Ψw"])
        result = 2 * func_approximation(etta, self.df30, etta_min, etta_max) * \
                 func_approximation(etta, self.df3n, etta_min, etta_max) * LaunchSettings().Sw
        return result

    def _recalc_special_params(self, special_params, a):
        gamma = LaunchSettings().gamma
        alpha_n = LaunchSettings().alpha_n
        K = LaunchSettings().K
        Sw = LaunchSettings().Sw

        A_in_4 = 2 * ((gamma - 1) ** 2) * ((gamma + 1) ** 3) * (a ** 2) * special_params["C"] / \
                 (9 * gamma * self.P10[0] * (self.V10[0] ** 2))
        special_params["A"] = A_in_4 ** 0.25
        special_params["B"] = 9 * gamma * (special_params["A"] ** 2) * self.P10[0] / (8 * (gamma + 1))
        special_params["Qn"] = alpha_n * (self.P1n[0] / self.P10[0] + (2 / 3) * (4 * self.n + 3))
        special_params["Ψw"] = - 8 * LaunchSettings().K * special_params["B"] / (LaunchSettings().gamma - 1) / \
                               LaunchSettings().Sw * (1 + special_params["Qn"] / (4 * self.n + 1))
