from source.utils.observer import PostManager, Keys
from source.structs.launch_settings import LaunchSettings
from source.solver.area_1_solver import Area1Solver
from source.solver.area_2_solver import Area2Solver
from source.solver.area_3_solver import Area3Solver
from source.solver.eigenvalue_finder import EigenValueFinder
from source.BaseManager.result_storage import ResultStorage


class SelfSimilarSolver:
    """Solver for self-similar task"""

    def __init__(self, x):
        self.n_max = 100  # expected maximum of eigenvalue
        self.n_min = 1.0  # expected minimum of eigenvalue
        self.n_shift_coeff = 0.2  # coefficient for shifting eigenvalue expected range in each iteration
        self.n_curr = self.n_max  # current checked eigenvalue
        self.N = LaunchSettings().N

        self.ksi1 = [i / self.N for i in range(0, self.N + 1)]

        self.solver1 = None
        self.solver2 = None
        self.solver3 = None

        self.eta3 = [0.0] * (self.N + 1)
        self.f30 = [0.0] * (self.N + 1)  # Stream function in area 3 in 0 approximation
        self.f3n = [0.0] * (self.N + 1)  # Stream function in area 3 in n approximation

        self.special_params = {"C": 1, "A": 1, "B": 1, "Qn": 1, "Ψw": 0, "x": x}
        self.special_params["Ψw"] = - 8 * LaunchSettings().K * self.special_params["B"] / (LaunchSettings().gamma - 1) \
                                    / LaunchSettings().Sw * (1 + self.special_params["Qn"] / (4 * self.n_curr + 1))

    @staticmethod
    def enough_accuracy(val1, val2):
        if val1 != 0 and abs(val1 - val2) / abs(val1) > LaunchSettings().accuracy:
            return False
        elif val1 == 0 and abs(val1 - val2) > LaunchSettings().accuracy:
            return False

        return True

    def _console_out(self, message):
        PostManager().notify(Keys().console_out, message)

    def _calc_for_specific_n(self, n):
        """
        Решение задачи с заданным собственным числом
        :param n: собственное число
        :return: интеграл L(n)
        """
        # return self._calc_for_specific_n_with_two_layers(n)  # для проверки работы в приближении двух слоев

        self.solver1 = Area1Solver(n)
        self.solver1.start()
        self.solver2 = Area2Solver(n, 0, 0)
        self.solver2.start(0, 0, 0, 0)
        self.solver3 = Area3Solver(n)
        L = EigenValueFinder(n, self.solver1, self.solver2, self.solver3).find(self.special_params)
        self.solver3.start(self.special_params)
        #L = EigenValueFinder(n, self.solver1, self.solver2, self.solver3).find(self.special_params)
        A_prev = self.special_params["A"] + 1

        while not self.enough_accuracy(A_prev, self.special_params["A"]):
            A_prev = self.special_params["A"]
            f20_min = - self.special_params["Ψw"] * self.solver3.f30_max
            f2n_min = - self.special_params["Ψw"] * self.solver3.f3n_max
            df20_min = self.solver3.df30_max  # LaunchSettings().Sw ** 0.5 * solver3.df30_max
            df2n_min = self.solver3.df3n_max  # LaunchSettings().Sw ** 0.5 * solver3.df3n_max
            self.solver2 = Area2Solver(n, self.solver3.etta2_min, self.solver3.delta_etta_3)
            self.solver2.start(f20_min, f2n_min, df20_min, df2n_min)
            L = EigenValueFinder(n, self.solver1, self.solver2, self.solver3).find(self.special_params)
            self.solver3 = Area3Solver(n)
            self.solver3.start(self.special_params)
            #L = EigenValueFinder(n, self.solver1, self.solver2, self.solver3).find(self.special_params)

        self._save_data()
        raise Exception("Костыль")
        return L

    def _calc_for_specific_n_with_two_layers(self, n):
        """
        Решение задачи с заданным собственным числом в приближении только двух слоев.
        :param n: собственное число
        :return: интеграл L(n)
        """
        self.solver1 = Area1Solver(n)
        self.solver1.start()
        self.solver2 = Area2Solver(n, 0, 0)
        self.solver2.start(0, 0, 0, 0)
        self.solver3 = Area3Solver(n)
        L = EigenValueFinder(n, self.solver1, self.solver2, self.solver3).find(self.special_params)
        A_prev = self.special_params["A"] + 1

        while not self.enough_accuracy(A_prev, self.special_params["A"]):
            A_prev = self.special_params["A"]
            coef = - 4 / (LaunchSettings().gamma - 1) / LaunchSettings().Sw * (self.special_params["B"] ** 0.5)
            f20_min = coef * LaunchSettings().K
            f2n_min = coef * LaunchSettings().K / (4 * n + 1)
            self.solver2 = Area2Solver(n, 0, 0)
            self.solver2.start(f20_min, f2n_min, 0, 0)
            L = EigenValueFinder(n, self.solver1, self.solver2, self.solver3).find(self.special_params)

        return L

    def _calc_for_shifted_n(self, prev_n, to_shift, expected_L_sign):
        """
        Shift eigenvalue n and solve problem to it
        :param prev_n: previous eigenvalue
        :param to_shift: sift coefficient
        :param expected_L_sign: expected sign of eigenvalue function L(n)
        :return: result of eigenvalue function L(n)
        """
        self.n_curr = prev_n + to_shift * (self.n_max - self.n_min)
        result = self._calc_for_specific_n(self.n_curr)
        return result

    def _main_iteration_cycle(self):
        """Iterations to select eigenvalue"""
        eigen_func_for_n_min_result = self._calc_for_specific_n(self.n_min)
        print(self.n_min, eigen_func_for_n_min_result)
        if abs(eigen_func_for_n_min_result) < LaunchSettings().accuracy:
            self.n_curr = self.n_min
            return

        eigen_func_for_n_max_result = self._calc_for_specific_n(self.n_max)
        print(self.n_max, eigen_func_for_n_max_result)
        if abs(eigen_func_for_n_max_result) < LaunchSettings().accuracy:
            self.n_curr = self.n_max
            return

        if eigen_func_for_n_max_result * eigen_func_for_n_min_result > 0:
            raise Exception("ERROR: bad targeting n")

        eigen_func_for_n_curr_result = 1000
        while abs(eigen_func_for_n_curr_result) > LaunchSettings().accuracy * 10:
            if abs(eigen_func_for_n_max_result) > abs(eigen_func_for_n_min_result):
                eigen_func_for_n_curr_result = self._calc_for_shifted_n(self.n_max, -self.n_shift_coeff,
                                                                        eigen_func_for_n_max_result)
                self.n_max = self.n_curr
                eigen_func_for_n_max_result = eigen_func_for_n_curr_result
                self._console_out("n = {}".format(self.n_curr))
                print(self.n_curr, eigen_func_for_n_curr_result)
            else:
                eigen_func_for_n_curr_result = self._calc_for_shifted_n(self.n_min, self.n_shift_coeff,
                                                                        eigen_func_for_n_min_result)
                self.n_min = self.n_curr
                eigen_func_for_n_min_result = eigen_func_for_n_curr_result
                self._console_out("n = {}".format(self.n_curr))
                print(self.n_curr, eigen_func_for_n_curr_result)

    def _save_data(self):
        ResultStorage().save_eigenvalue(self.n_curr)
        ResultStorage().save_data(zip(["ξ_1", "P10", "V10", "P1n", "V1n"],
                                      [self.ksi1, self.solver1.P10, self.solver1.V10,
                                       self.solver1.P1n, self.solver1.V1n]), "Zone_1")
        ResultStorage().save_data(zip(["η_2", "f20", "f2n", "S20", "S2n"],
                                      [self.solver2.eta2, self.solver2.f20, self.solver2.f2n,
                                       self.solver2.S20, self.solver2.S2n]), "Zone_2")
        ResultStorage().save_data(zip(["η_3", "f30", "f3n", "S30", "S3n"],
                                      [self.solver3.eta3, self.solver3.f30, self.solver3.f3n,
                                       self.solver3.S30, self.solver3.S3n]), "Zone_3")

    def start(self):
        try:
            self._console_out("---\n" + "Start main iteration cycle")
            self._main_iteration_cycle()
            self._save_data()
            self._console_out("Success")
        except Exception as e:
            print(e)
            self._console_out(str(e))

        self._console_out("---")


class MainSolver:
    """General solver of all the problems"""

    def __init__(self):
        self.x_profiles = (0.2, 0.4, 0.6, 0.8, 1)

    def start(self):
        PostManager().notify(Keys().console_out, "===\n" + "Start calculating")

        solver = SelfSimilarSolver(x=0.6)
        solver.start()

        PostManager().notify(Keys().console_out, "End calculating" + "\n===")
