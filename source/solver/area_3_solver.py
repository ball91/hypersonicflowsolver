from source.structs.launch_settings import LaunchSettings


class Area3Solver:
    def __init__(self, n):
        self.n = n
        self.N = LaunchSettings().N
        self.etta_min = 0
        self.etta_max = LaunchSettings().etta_max_3
        self.h = (self.etta_max - self.etta_min) / self.N

        self.f30 = [0.0] * (self.N + 1)  # Stream function in area 3 in 0 approximation
        self.f3n = [0.0] * (self.N + 1)  # Stream function in area 3 in n approximation
        self.S30 = [LaunchSettings().Sw] * (self.N + 1)
        self.S3n = [0.0] * (self.N + 1)

        self.u30 = [0] * (self.N + 1)
        self.u3n = [0] * (self.N + 1)

        self.delta_etta_3 = 0  # толщина третьего слоя по автомодельной координате
        self.etta2_min = 0  # отступ 2-ой области от нулевой линии тока вглубь 3-его слоя
        self.f30_max = 0  # значение f30 на границе с погранслоем
        self.f3n_max = 0  # значение f3n на границе с погранслоем
        self.df30_max = 0  # значение df30 на границе с погранслоем
        self.df3n_max = 0  # значение df3n на границе с погранслоем

        self.i_max = 100  # индекс границы зоны

        # Вычисляем набор координат, для последующего вывода в файл
        self.eta3 = [0.0] * (self.N + 1)
        for i in range(0, self.N + 1):
            self.eta3[i] = self.etta_min + i * self.h

    def start(self, special_params):
        self._set_targeting_values(special_params)
        self._calc_0_values()
        self._calc_n_value()
        self._find_boundary_params(special_params)
        self._recalc_eta3(special_params)

    def _set_targeting_values(self, special_params):
        B = special_params["B"]
        C = special_params["C"]
        Qn = special_params["Qn"]
        Sw = LaunchSettings().Sw
        g = LaunchSettings().gamma

        self.f30[0] = - 0.5 * (B * C) ** (-0.5) / (1 + Qn / (4 * self.n + 1))
        self.f3n[0] = self.f30[0] / (4 * self.n + 1)
        self.f30[1] = self.f30[0]
        self.f3n[1] = self.f3n[0]

    def _calc_0_values(self):
        g = LaunchSettings().gamma
        for i in range(1, self.N):
            a = self.f30[i]
            b = (1 - g) / g
            self.f30[i + 1] = 2 * self.f30[i] - self.f30[i - 1] + \
                              b / a * (self.h ** 2 - (self.f30[i] - self.f30[i - 1]) ** 2)

        for i in range(0, self.N):
            self.u30[i + 1] = (self.f30[i + 1] - self.f30[i]) / self.h

    def _calc_n_value(self):
        du0 = [0] * (self.N + 1)
        for i in range(0, self.N):
            du0[i] = (self.u30[i + 1] - self.u30[i]) / self.h
        du0[self.N] = du0[self.N - 1]

        g = LaunchSettings().gamma
        for i in range(1, self.N):
            a = self.f30[i]
            b = - (4 * self.n + 2 * (g - 1) / g) * self.u30[i]
            c = (4 * self.n + 1) * du0[i]
            d = 2 * (g - 1) / g * (self.n - 0.5) * (1 - self.u30[i] ** 2)
            self.f3n[i + 1] = ((4 * a - 2 * c * self.h ** 2) * self.f3n[i] +
                               (b * self.h - 2 * a) * self.f3n[i - 1] +
                               2 * d * self.h ** 2) / (2 * a + b * self.h)

        for i in range(0, self.N):
            self.u3n[i + 1] = (self.f3n[i + 1] - self.f3n[i]) / self.h

    def _find_boundary_params(self, special_params):
        Qn = special_params["Qn"]
        x = special_params["x"]

        def psi(i):
            return self.f30[i] + Qn * x ** self.n * self.f3n[i]

        for i in range(1, self.N):
            if (psi(i) * psi(i + 1) < 0):
                j = int(i * 1.0)
                self.i_max = j
                self.delta_etta_3 = self.etta_min + self.h * j
                self.etta2_min = self.delta_etta_3 - (self.etta_min + self.h * i)
                ### Перевод в размерность 2-ой области ###
                self.delta_etta_3 *= LaunchSettings().Sw ** 0.5 * (- special_params["Ψw"])
                self.etta2_min *= LaunchSettings().Sw ** 0.5 * (- special_params["Ψw"])
                ###
                self.f30_max = self.f30[j]
                self.f3n_max = self.f3n[j]
                self.df30_max = self.u30[j]
                self.df3n_max = self.u3n[j]
                return

        raise Exception("Can not find boundary params in 3 layer")

    def _recalc_eta3(self, special_params):
        koef = LaunchSettings().Sw ** 0.5 * (- special_params["Ψw"])
        for i in range(0, self.N + 1):
            self.eta3[i] *= koef
            self.f30[i] *= - special_params["Ψw"]
            self.f3n[i] *= - special_params["Ψw"]
        # Обрезаем решение до нужного кол-ва точек
        self.f30 = self.f30[:self.i_max + 1]
        self.f3n = self.f3n[:self.i_max + 1]
        self.eta3 = self.eta3[:self.i_max + 1]
        self.S30 = self.S30[:self.i_max + 1]
        self.S3n = self.S3n[:self.i_max + 1]
        # Костыль
        self.df30_max = (self.f30[-1] - self.f30[-2]) / self.h
        self.df3n_max = (self.f3n[-1] - self.f3n[-2]) / self.h
