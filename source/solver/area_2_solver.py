from source.structs.launch_settings import LaunchSettings
from source.utils.progonka2 import ProgonkaByPoints


class Area2Solver:
    def __init__(self, n, etta_min, delta_etta_3):
        self.n = n

        self.u0 = [0.0] * (LaunchSettings().N + 1)  # derivative of f20
        self.un = [0.0] * (LaunchSettings().N + 1)  # derivative of f2n

        self.N = LaunchSettings().N
        self.etta_min = etta_min
        self.etta_max = LaunchSettings().etta_max_2
        self.h = (self.etta_max - self.etta_min) / self.N

        self.f20 = [0.0] * (self.N + 1)  # Stream function in area 2 in 0 approximation
        self.f2n = [0.0] * (self.N + 1)  # Stream function in area 2 in n approximation
        self.S20 = [0.0] * (self.N + 1)  # Entropy in area 2 in 0 approximation
        self.S2n = [0.0] * (self.N + 1)  # Entropy in area 2 in n approximation

        # Вычисляем набор координат, для последующего вывода в файл
        self.eta2 = [0.0] * (self.N + 1)
        for i in range(0, self.N + 1):
            self.eta2[i] = delta_etta_3 + i * self.h

    def start(self, f20_min, f2n_min, df20_min, df2n_min):
        self._set_targeting_values(f20_min, f2n_min, df20_min, df2n_min)
        self._calc_0_values()
        self._calc_n_value()
        self._restart_if_needed(f20_min, f2n_min, df20_min, df2n_min)

    def _set_targeting_values(self, f20_min, f2n_min, df20_min, df2n_min):
        #df20_min = 0  # по Нейланду
        #df2n_min = 0  # по Нейланду
        for i in range(0, self.N + 1):
            self.f20[i] = f20_min + i * self.h
            self.f2n[i] = f2n_min + i * self.h
            self.S20[i] = LaunchSettings().Sw + i * (1 - LaunchSettings().Sw) / self.N
            self.S2n[i] = 0
            self.u0[i] = df20_min + i * self.h
            self.un[i] = df2n_min
        self.S20[0] = LaunchSettings().Sw
        self.S20[self.N] = 1
        self.S2n[0] = 0
        self.S2n[self.N] = 0

    def _calc_0_values(self):
        f20_prev = [0] * (self.N + 1)
        S20_prev = [0] * (self.N + 1)
        while not self._enough_accuracy(self.f20, f20_prev) or not self._enough_accuracy(self.S20, S20_prev):
            f20_prev = self.f20.copy()
            S20_prev = self.S20.copy()
            self._calc_u0()
            self._calc_f20()
            self._calc_S20()

    def _calc_u0(self):
        self.u0[self.N] = 1
        for i in range(1, self.N):
            self.u0[i] = (self.f20[i + 1] - self.f20[i]) / self.h

        p = self.f20
        q = [0] * (self.N + 1)
        f = [None] * (self.N + 1)
        gamma = LaunchSettings().gamma
        for i in range(0, self.N + 1):
            f[i] = (self.u0[i] * self.u0[i] - self.S20[i]) * (gamma - 1) / gamma

        self.u0 = ProgonkaByPoints(p, q, f, self.u0[0], self.u0[self.N], self.etta_min, self.etta_max, self.N).calc()

    def _calc_f20(self):
        for i in range(0, self.N):
            self.f20[i + 1] = self.f20[i] + self.u0[i] * self.h

    def _calc_S20(self):
        p = self.f20
        q = [0] * (self.N + 1)
        f = [0] * (self.N + 1)

        self.S20 = ProgonkaByPoints(p, q, f, LaunchSettings().Sw, 1, self.etta_min, self.etta_max, self.N).calc()

    def _calc_n_value(self):
        f2n_prev = [0] * (self.N + 1)
        S2n_prev = [1] * (self.N + 1)
        while not self._enough_accuracy(self.f2n, f2n_prev) or not self._enough_accuracy(self.S2n, S2n_prev):
            f2n_prev = self.f2n.copy()
            S2n_prev = self.S2n.copy()
            self._calc_un()
            self._calc_f2n()
            self._calc_S2n()

    def _calc_un(self):
        du0 = [0] * (self.N + 1)
        for i in range(0, self.N):
            du0[i] = (self.u0[i + 1] - self.u0[i]) / self.h
        du0[self.N] = du0[self.N - 1]

        ddu0 = [0] * (self.N + 1)
        for i in range(0, self.N):
            ddu0[i] = (du0[i + 1] - du0[i]) / self.h
        ddu0[self.N] = ddu0[self.N - 1]

        gamma = LaunchSettings().gamma
        p = self.f20
        q = [None] * (self.N + 1)
        for i in range(0, self.N + 1):
            q[i] = - (4 * self.n + 2 * (gamma - 1) / gamma) * self.u0[i]
        f = [None] * (self.N + 1)
        for i in range(0, self.N + 1):
            f[i] = - (4 * self.n + 1) * du0[i] * self.f2n[i] - \
                   (gamma - 1) / gamma * self.S2n[i] + \
                   2 * (gamma - 1) / gamma * (self.n - 0.5) * (self.S20[i] - self.u0[i] ** 2) - ddu0[i]

        self.un = ProgonkaByPoints(p, q, f, self.un[0], self.un[self.N], self.etta_min, self.etta_max, self.N).calc()

    def _calc_f2n(self):
        for i in range(0, self.N):
            self.f2n[i + 1] = self.f2n[i] + self.un[i] * self.h

    def _calc_S2n(self):
        dS0 = [0] * (self.N + 1)
        for i in range(0, self.N):
            dS0[i] = (self.S20[i + 1] - self.S20[i]) / self.h
        dS0[self.N] = dS0[self.N - 1]

        ddS0 = [0] * (self.N + 1)
        for i in range(0, self.N):
            ddS0[i] = (dS0[i + 1] - dS0[i]) / self.h
        ddS0[self.N] = ddS0[self.N - 1]

        p = self.f20
        q = [None] * (self.N + 1)
        for i in range(0, self.N + 1):
            q[i] = - 4 * self.n * self.u0[i]
        f = [None] * (self.N + 1)
        for i in range(0, self.N + 1):
            f[i] = - (4 * self.n + 1) * self.f2n[i] * dS0[i] - ddS0[i]

        self.S2n = ProgonkaByPoints(p, q, f, self.S2n[0], self.S2n[self.N], self.etta_min, self.etta_max, self.N).calc()

    def _restart_if_needed(self, f20_min, f2n_min, df20_min, df2n_min):
        """Если f(0) != 0, то перерасчитываем etta_min и перезапускаем расчет"""
        new_etta_min = 0
        for i in range(0, self.N):
            if self.f20[i] > 0:
                new_etta_min = - i * self.h
                break

        #print(abs(new_etta_min - self.etta_min))
        if abs(new_etta_min - self.etta_min) > 0.1:
            self.etta_min = new_etta_min
            self.h = (self.etta_max - self.etta_min) / self.N
            self.start(f20_min, f2n_min, df20_min, df2n_min)


    @staticmethod
    def _enough_accuracy(list1, list2):
        for v1, v2 in zip(list1, list2):
            if v1 != 0 and abs(v1 - v2) / abs(v1) > LaunchSettings().accuracy:
                return False
            elif v1 == 0 and abs(v1 - v2) > LaunchSettings().accuracy:
                return False

        return True
