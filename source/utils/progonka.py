class Progonka:
    """
    Progonka method solver for equation in form:
    y'' + p(x) y' + q(x) y = f(x)
    x_min < x < x_max
    y(x_min) = A
    y(x_max) = B
    """

    def __init__(self, p, q, f, A, B, x_min, x_max, N):
        """
        :param p: p(x)
        :param q: q(x)
        :param f: f(x)
        :param A: const
        :param B: const
        :param x_min: const
        :param x_max: const
        :param N: the number of nodes in grid
        """
        self.p = p
        self.q = q
        self.f = f
        self.A = A
        self.B = B
        self.x_min = x_min
        self.x_max = x_max
        self.N = N
        self.h = (x_max - x_min) / N
        self.a = [None] * (self.N + 1)
        self.b = [None] * (self.N + 1)
        self.c = [None] * (self.N + 1)
        self.l = [None] * (self.N + 1)
        self.t = [None] * (self.N + 1)
        self.y = [None] * (self.N + 1)

    def _prepare_coef(self):
        """
        Prepare a_n, b_n, c_n in difference scheme:
        a_n y_(n-1) + b_n y_n + c_n y_(n+1) = f_n
        :return: None
        """
        for n in range(1, self.N):
            x_n = self.x_min + self.h * n
            self.a[n] = 1 / (self.h ** 2) - self.p(x_n) / (2 * self.h)
            self.b[n] = self.q(x_n) - 2 / (self.h ** 2)
            self.c[n] = 1 / (self.h ** 2) + self.p(x_n) / (2 * self.h)

    def _direct_progonka(self):
        """
        Direct progonka to find l_n and t_n, that are used in equation:
        y_n = t_n - l_n y_(n+1)
        :return: None
        """
        self.l[0] = 0
        self.t[0] = self.A
        for n in range(1, self.N):
            x_n = self.x_min + self.h * n
            self.l[n] = self.c[n] / (self.b[n] - self.l[n - 1] * self.a[n])
            self.t[n] = (self.f(x_n) - self.t[n - 1] * self.a[n]) / (self.b[n] - self.l[n - 1] * self.a[n])

    def _inverse_progonka(self):
        """
        Inverse progonka to find y_n
        :return: None
        """
        self.y[0] = self.A
        self.y[self.N] = self.B
        for n in range(self.N - 1, 0, -1):
            self.y[n] = self.t[n] - self.l[n] * self.y[n + 1]

    def calc(self):
        """
        Solve the equation
        :return: y (array[N+1])
        """
        self._prepare_coef()
        self._direct_progonka()
        self._inverse_progonka()
        return self.y
