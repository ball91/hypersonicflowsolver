from abc import ABC, abstractmethod
from source.utils.singleton import singleton


@singleton
class PostManager():
    def __init__(self):
        self._subscibers = dict()

    def notify(self, key, message):
        if key in self._subscibers:
            for subscriber in self._subscibers[key]:
                subscriber.update(key, message)

    def subscribe(self, key, subscriber):
        if key not in self._subscibers:
            self._subscibers[key] = []
        if subscriber not in self._subscibers[key]:
            self._subscibers[key].append(subscriber)

    def unsubscribe(self, key, subscriber):
        if key not in self._subscibers:
            return
        if subscriber not in self._subscibers[key]:
            return

        self._subscibers[key].remove(subscriber)
        if len(self._subscibers[key]) == 0:
            del self._subscibers[key]


class Subscriber(ABC):
    @abstractmethod
    def update(self, key, message):
        pass

    def subscribe(self, key):
        PostManager().subscribe(key, self)

    def unsubscribe(self, key):
        PostManager().unsubscribe(key, self)


@singleton
class Keys:
    def __init__(self):
        self.console_out = "console_out"
