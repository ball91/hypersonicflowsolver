class RungeKutta:
    """
    Runge Kutta method for equation in form:
    y' = f(x,y)
    x_min < x < x_max
    y(x_min or x_max) = A
    """

    def __init__(self, f, A, x_min, x_max, N):
        """
        :param f: f(x,y)
        :param A: const
        :param x_min: const
        :param x_max: const
        :param N: the number of nodes in grid
        """
        self.f = f
        self.A = A
        self.x_min = x_min
        self.x_max = x_max
        self.N = N
        self.h = (x_max - x_min) / N
        self.y = [None] * (self.N + 1)

    def calc(self, reverse=False):
        """
        Solve the equation
        :param reverse: reverse calculation from right to left
        :return: y (array[N+1])
        """
        if reverse:
            self.y[0] = self.A
            for i in range(0, self.N):
                x_i = -self.x_max + self.h * i
                p1 = -self.f(-x_i, self.y[i])
                p2 = -self.f(-x_i - self.h / 2, self.y[i] + p1 * self.h / 2)
                p3 = -self.f(-x_i - self.h / 2, self.y[i] + p2 * self.h / 2)
                p4 = -self.f(-x_i - self.h, self.y[i] + p3 * self.h)
                self.y[i + 1] = self.y[i] + self.h * (p1 + 2 * p2 + 2 * p3 + p4) / 6

            self.y.reverse()
        else:
            self.y[0] = self.A
            for i in range(0, self.N):
                x_i = self.x_min + self.h * i
                p1 = self.f(x_i, self.y[i])
                p2 = self.f(x_i + self.h / 2, self.y[i] + p1 * self.h / 2)
                p3 = self.f(x_i + self.h / 2, self.y[i] + p2 * self.h / 2)
                p4 = self.f(x_i + self.h, self.y[i] + p3 * self.h)
                self.y[i + 1] = self.y[i] + self.h * (p1 + 2 * p2 + 2 * p3 + p4) / 6

        return self.y
