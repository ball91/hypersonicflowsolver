class Simpson:
    """
    Simpson method to solve integral in form:
    J = ∫_a^b f(x) dx
    """

    def __init__(self, f, a, b, N):
        """
        :param f: f(x)
        :param a: x_min
        :param b: x_max
        :param N: the number of nodes in grid
        """
        self.f = f
        self.a = a
        self.b = b
        self.N = N
        self.h = (b - a) / (2 * self.N)

    def calc(self):
        """
        Solve integral
        :return: result of integration
        """
        sum_res1 = 0
        sum_res2 = 0

        for i in range(1, self.N):
            sum_res1 += self.f(self.a + (2 * i - 1) * self.h)
            sum_res2 += self.f(self.a + 2 * i * self.h)
        sum_res1 += self.f(self.a + (2 * self.N - 1) * self.h)

        res = self.h / 3.0 * (self.f(self.a) + 4 * sum_res1 + 2 * sum_res2 + self.f(self.b))
        return res
