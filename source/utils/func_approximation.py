from source.structs.launch_settings import LaunchSettings


def func_approximation(x, values, x_min, x_max):
    """Simulate function basing on values list"""
    h = (x_max - x_min) / LaunchSettings().N
    if (x <= x_min):
        return values[0]
    elif (x >= x_max):
        return values[LaunchSettings().N]

    j = int((x - x_min) // h)
    shift = (x - x_min) % h
    return values[j] + shift / h * (values[j+1] - values[j])
