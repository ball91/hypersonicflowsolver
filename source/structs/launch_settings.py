from source.utils.singleton import singleton
from source.get_launch_dir import get_launch_dir


@singleton
class LaunchSettings:
    def __init__(self):
        self.working_dir = get_launch_dir()  # directory for store data
        self.mach_inf = 6.0  # Mach number in infinite
        self.gamma = 1.4  # adiabatic index
        self.Sw = 0.75  # enthalpy on the plate
        self.alpha_n = 0.0  # αn - influence of not self-similar solution
        self.K = 0.0  # injection speed coefficient
        self.N = 1000  # the number of nodes in grid
        self.etta_max_2 = 27.0  # maximum coordinate in the second zone
        self.etta_min_2 = -27.0  # minimum coordinate in the second zone
        self.etta_max_3 = 10.0  # maximum coordinate in the third zone
        self.accuracy = 0.01  # accuracy of finding solution

    def pack_to_tuple(self):
        """Pack launch settings to tuple"""
        return (self.working_dir,
                self.mach_inf,
                self.gamma,
                self.Sw,
                self.alpha_n,
                self.K,
                self.N,
                self.etta_max_2,
                self.etta_min_2,
                self.etta_max_3,
                self.accuracy)

    def unpack_from_tuple(self, data):
        """Unpack launch settings from tuple"""
        self.working_dir, \
        self.mach_inf, \
        self.gamma, \
        self.Sw, \
        self.alpha_n, \
        self.K, \
        self.N, \
        self.etta_max_2, \
        self.etta_min_2, \
        self.etta_max_3, \
        self.accuracy = data
