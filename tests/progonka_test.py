from source.utils.progonka import Progonka
from math import sinh, cosh

a = -1
x_min = 0
x_max = 1
y_min = 0
y_max = 0
N = 30

def p(x):
    return 0


def q(x):
    return a


def f(x):
    return 1


def y(x):
    C1 = (cosh(abs(a)) - 1) / a / sinh(abs(a))
    C2 = - 1 / a
    return C1 * sinh(abs(a) ** 0.5 * x) + C2 * cosh(abs(a) ** 0.5 * x) + 1 / a


def test():
    y_res = Progonka(p, q, f, y_min, y_max, x_min, x_max, N).calc()
    max_diff = 0
    for i in range(0, N + 1):
        x = i / N
        diff = abs(y(x) - y_res[i])
        if max_diff < diff:
            max_diff = diff

    print(max_diff)


test()