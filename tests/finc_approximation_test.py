from source.utils.func_approximation import func_approximation
from source.structs.launch_settings import LaunchSettings

x_min = -10
x_max = 10
N = LaunchSettings().N

def f(x):
    return (x - 1) ** 2


def test():
    massive = [0] * (N + 1)
    for n in range(0, N + 1):
        x = x_min + n / N * (x_max - x_min)
        massive[n] = f(x)

    x1 = -9.999
    f_accurate_1 = f(x1)
    f_approximation_1 = func_approximation(x1, massive, x_min, x_max)
    x2 = 0.003
    f_accurate_2 = f(x2)
    f_approximation_2 = func_approximation(x2, massive, x_min, x_max)

    print(abs(f_accurate_1 - f_approximation_1))
    print(abs(f_accurate_2 - f_approximation_2))


test()