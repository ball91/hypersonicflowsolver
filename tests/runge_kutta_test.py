from source.utils.runge_kutta import RungeKutta
from math import exp


def f(x, y):
    return x * exp(- x ** 2) - 2 * x * y


def test():
    x_min = 0
    x_max = 1
    y_min = 10
    y_max = 10.5 / exp(1)
    N = 40

    y = RungeKutta(f, y_max, x_min, x_max, N).calc(reverse=True)

    max_error = 0
    for i in range(0, N + 1):
        x = i / N
        y_accurate = (x ** 2 / 2 + y_min) * exp(- x ** 2)
        y_approximate = y[i]
        error = abs(y_accurate - y_approximate)
        max_error = error if error > max_error else max_error
    print(max_error)


test()
