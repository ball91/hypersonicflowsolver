from source.utils.simpson import Simpson
from math import exp


def f(x):
    return exp(x)


def test():
    N = 200
    a = -0.5
    b = 10
    I_accuracy = exp(b) - exp(a)
    I_numeric = Simpson(f, a, b, N // 2).calc()

    print(I_accuracy, I_numeric, abs(I_accuracy - I_numeric))


test()
